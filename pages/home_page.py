from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.base_page import BasePage


class HomePage(BasePage):
    page_title = "Home | Salesforce"

    # Locators
    _breadcrumb_detail_locator = (By.CSS_SELECTOR, ".breadcrumbDetail")
    _sales_button_locator = (By.XPATH, "//p[text()='Sales']")
    _waffle_button_locator = (By.CSS_SELECTOR, ".slds-icon-waffle")

    # Navigation methods
    def go_to_sales_page(self):
        self.driver.find_element(*self._waffle_button_locator).click()

        self.driver.find_element(*self._sales_button_locator).click()

        from pages.sales_home_page import SalesHomePage
        return SalesHomePage(self.driver)
