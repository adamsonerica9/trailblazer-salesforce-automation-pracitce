from selenium.common import NoSuchElementException
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver import Remote
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.sales_navbar import SalesNavbar


class SalesAccountPage(SalesNavbar):
    def __init__(self, driver, account_name):
        # Sets page object title to one with current account viewed
        self.account_name = account_name
        self.page_title = f"{account_name} | Salesforce"

        super().__init__(driver)

    # Locators
    _button_edit_locator = (By.XPATH, "//button[@name='Edit']")
    _details_tab_locator = (By.XPATH, "//a[@data-tab-value='detailTab']")
    _details_form_element_locator = (
        By.XPATH, "//*[@data-component-id='force_detailPanel']//div[contains(@class, 'slds-form-element_edit')]")
    _details_form_element_field_label_locator = (
        By.CSS_SELECTOR, "[aria-labelledby='detailTab__item'] .slds-form-element")

    # Navigation methods
    def click_button_edit(self):
        self.driver.find_element(*self._button_edit_locator).click()

        return EditModal(self.driver, self.account_name)

    def click_tab_details(self):
        self.driver.find_element(*self._details_tab_locator).click()

    def get_details(self):
        form_elements = self.driver.find_elements(*self._details_form_element_locator)
        details = {}

        for form_element in form_elements:
            field_label = form_element.find_element(By.CSS_SELECTOR, ".test-id__field-label").text
            form_element_control = form_element.find_element(By.CSS_SELECTOR, ".slds-form-element__control span")

            details[field_label.lower()] = form_element_control.text

        details['parent account'] = details['parent account'].lstrip("Open ").rstrip(" Preview")

        return details


class EditModal:
    def __init__(self, driver: Remote, account_name):
        self.driver = driver

        # Gets all modal windows
        modal_windows = self.driver.find_elements(By.CSS_SELECTOR, "div.actionBody")

        for modal_window in modal_windows:
            # Searches for the right modal window
            header = modal_window.find_element(By.CSS_SELECTOR, "h2").text

            if f"Edit {account_name}" in header:
                self.edit_modal_element = modal_window
                return

        # If window not found, raise exception
        raise NoSuchElementException(f"Could not find {account_name} edit window")
    
    # Locators
    _inputs_except_combobox_lookups_locator = (
        By.CSS_SELECTOR, ".slds-input:not(.slds-combobox__input),button.slds-combobox__input-value,.slds-textarea")
    _inputs_combobox_lookups_label_locator = (
        By.XPATH, "//lightning-grouped-combobox//label")  # Combobox lookups work different
    _inputs_combobox_lookups_input_locator = (  # Tried to locate them with proper methods but none worked
        By.XPATH, "//lightning-grouped-combobox//input")

    _button_save_locator = (By.CSS_SELECTOR, "button[name='SaveEdit']")

    _loading_spinner = (By.CSS_SELECTOR, ".forceModalSpinner")

    # Get element methods
    def _get_form_input_elements(self):
        """
        Gets all form elements with their names
        :return: Dictionary with keys as names and values as form elements
        """
        form_inputs = self.edit_modal_element.find_elements(*self._inputs_except_combobox_lookups_locator)

        form_input_dict = {}

        for form_input in form_inputs:
            label = form_input.accessible_name.lstrip('*')  # Gets input label and trims the '*' required symbol
            label = label.split(',')[0]  # Dropdowns have name and value separated by a comma, get only name
            label = label.lower()

            form_input_dict[label] = form_input

        # Handle combobox lookups
        combobox_lookup_labels = self.edit_modal_element.find_elements(*self._inputs_combobox_lookups_label_locator)
        combobox_lookup_inputs = self.edit_modal_element.find_elements(*self._inputs_combobox_lookups_input_locator)

        for i, combobox_lookup_label in enumerate(combobox_lookup_labels):
            label = combobox_lookup_label.text.lower()

            form_input_dict[label] = combobox_lookup_inputs[i]

        return form_input_dict

    # Navigation methods
    def _input_combobox_lookup_value(self, combobox_element, value):
        combobox_element.send_keys(Keys.BACKSPACE)

        if not value:
            return

        # Perform lookup if value is not empty
        combobox_element.send_keys(value)

        try:
            value_to_choose = combobox_element.find_element(
                By.XPATH, f"//*[@title='{value}']/strong")
        except NoSuchElementException as e:
            raise NoSuchElementException(
                f"No value {value} found in combobox lookup {combobox_element.accessible_name}") from e

        value_to_choose.click()

    def _input_combobox_dropdown_value(self, dropbox_element, value):
        if value.lower == "none":
            value = "--None--"

        dropbox_element.click()

        try:
            dropbox_element.find_element(By.XPATH, f"//span[text()='{value.capitalize()}']").click()
        except NoSuchElementException as e:
            raise NoSuchElementException(
                f"No value {value} found in dropbox {dropbox_element.accessible_name}") from e

    def _input_textbox_value(self, textbox_element, value):
        textbox_element.clear()
        textbox_element.send_keys(value)

    def input_values(self, values: dict[str, str]):
        form_input_elements = self._get_form_input_elements()

        for key, value in values.items():
            try:
                element = form_input_elements[key.lower()]
            except KeyError as e:
                raise KeyError(f"No property field {key}") from e

            match element.aria_role:  # Get element HTML tag
                case 'textbox':
                    self._input_textbox_value(element, value)
                case 'combobox':
                    # There are two types of comboboxes - simple dropdown and one with lookup
                    if element.tag_name == 'input':
                        self._input_combobox_lookup_value(element, value)
                    else:
                        self._input_combobox_dropdown_value(element, value)

    def click_button_save(self):
        self.edit_modal_element.find_element(*self._button_save_locator).click()

        # Wait for server to save the details
        WebDriverWait(self.driver, 5).until_not(
            expected_conditions.url_contains("edit"))
