from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.sales_navbar import SalesNavbar


class SalesAccountListPage(SalesNavbar):

    def __init__(self, driver):
        # Sets the page object title according to the default list view
        WebDriverWait(driver, 10).until(
            expected_conditions.presence_of_element_located(self._account_view_title_locator))

        list_view = driver.title.split('|', 1)[0]

        self.page_title = f"{list_view}| Accounts | Salesforce"

        super().__init__(driver)

    # Locators
    _account_view_title_locator = (By.XPATH, "//span[text()='Accounts' and @class='slds-assistive-text']")
    _selected_list_locator = (By.CSS_SELECTOR, ".selectedListView")
    _all_accounts_list_option_locator = (By.XPATH, "//span[text()='All Accounts']")
    _loading_spinner_locator = (By.CSS_SELECTOR, ".slds-spinner_container.slds-hide")
    
    # Dynamic locators
    def _account_locator(self, account_name):
        return By.CSS_SELECTOR, f"a[title='{account_name}']"

    # Navigation methods
    def change_list_view_to_all_accounts(self):
        self.driver.find_element(*self._selected_list_locator).click()

        self.driver.find_element(*self._all_accounts_list_option_locator).click()

    def click_account(self, account_name):
        try:
            WebDriverWait(self.driver, 10).until(expected_conditions.presence_of_element_located(
                self._loading_spinner_locator
            ))
        except TimeoutException as e:
            raise TimeoutException("Account list failed to load") from e

        self.driver.find_element(*self._account_locator(account_name)).click()

        from pages.sales_account_page import SalesAccountPage
        return SalesAccountPage(self.driver, account_name)
