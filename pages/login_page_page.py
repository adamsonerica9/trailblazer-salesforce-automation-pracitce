from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class LogInPage(BasePage):
    page_title = "Login | Salesforce"

    # Locators
    _username_locator = (By.ID, "username")
    _password_locator = (By.ID, "password")
    _login_button_locator = (By.ID, "Login")

    # Navigation methods
    def log_in(self, username, password):
        self.driver.find_element(*self._username_locator).send_keys(username)
        self.driver.find_element(*self._password_locator).send_keys(password)
        self.driver.find_element(*self._login_button_locator).click()

        from pages.home_page import HomePage
        return HomePage(self.driver)
