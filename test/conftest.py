"""
Pytest file containing fixtures to configure what happens after and before tests
"""
import json
import pytest
import selenium.webdriver

from pages.login_page_page import LogInPage


@pytest.fixture
def credentials():
    try:
        with open('test_data/credentials.json', 'r') as credentials_file:
            credentials = json.load(credentials_file)
    except FileNotFoundError as e:
        with open('test_data/credentials.json', 'w') as credentials_file:
            # Initialize credentials file
            credentials_initalize = {
                "username": "",
                "password": ""
            }
            credentials_file.write(json.dumps(credentials_initalize))

        raise FileNotFoundError("Enter login credentials in test_data/credentials.json") from e

    # Checks if credentials are not empty
    assert credentials['username'], "Please provide username in test_data/credentials.json"
    assert credentials['password'], "Please provide password in test_data/credentials.json"

    return credentials


@pytest.fixture
def config(autouse=True):
    # Loads the test config which specifies the browser used for tests
    # and implicit wait time

    with open('test_config.json') as config_file:
        config = json.load(config_file)

    assert config['browser'] in ['Edge', 'Chrome']  # Checks if browser is supported
    assert isinstance(config['implicit_wait'], int)
    assert config['implicit_wait'] > 0

    return config


@pytest.fixture
def driver(config, credentials):
    # Sets up the browser and implicit wait time specified in the test config file

    match config['browser']:
        case 'Edge':
            driver = selenium.webdriver.Edge()
        case 'Chrome':
            driver = selenium.webdriver.Chrome()
        case _:
            raise Exception(f'Browser "{config["browser"]}" is not supported')

    driver.implicitly_wait(config['implicit_wait'])

    driver.get(config['start_url'])

    current_page = LogInPage(driver).log_in(credentials['username'], credentials['password'])

    current_page.go_to_sales_page()

    yield driver  # Gives control to the test method

    # This performs after the test method is done
    driver.quit()
