from collections import namedtuple

import pytest
import re
import json

from pages.sales_home_page import SalesHomePage


def read_edit_account_parameters():
    with open('test_data/edit_account_parameters.json') as edit_values_file:
        json_file = json.load(edit_values_file)

    test_parameters = []
    for jsonObject in json_file:
        try:
            account = jsonObject["account"]
        except KeyError as e:
            raise KeyError("No account for edition is specified in edit_account_parameters.json") from e

        try:
            edit_values = jsonObject["edit_values"]
        except KeyError as e:
            raise KeyError("No edit values specified in edit_account_parameters.json") from e

        EditAccountParameters = namedtuple("EditAccountParameters", "account, edit_values")
        test_parameters.append(EditAccountParameters(account, edit_values))

    assert test_parameters, "No parameters specified in edit_account_parameter.json"

    return test_parameters


@pytest.mark.parametrize("edit_parameters", read_edit_account_parameters())
def test_edit_account(driver, edit_parameters):
    account = edit_parameters.account
    edit_values = edit_parameters.edit_values

    current_page = SalesHomePage(driver)

    current_page = current_page.navbar_go_to_sales_accounts_page()

    current_page.change_list_view_to_all_accounts()

    current_page = current_page.click_account(account)

    edit_modal_window = current_page.click_button_edit()

    edit_modal_window.input_values(edit_values)
    edit_modal_window.click_button_save()

    current_page.click_tab_details()

    details = current_page.get_details()

    compare_edit_values_with_details(edit_values, details)


def compare_edit_values_with_details(edit_values, details):
    for key in edit_values:
        key_lowercase = key.lower()
        if any(word in key_lowercase for word in ["shipping", "billing"]):
            # Addresses are compared differently due to the difference in data formatting
            address_type = key_lowercase.split()[0]
            details_address = details[f"{address_type} address"]
            # Checks if address part from edit_values is in full address of details_address
            match = re.match(r"\b" + edit_values[key] + r"\b", details_address)
            assert match is not None, f"Wrong {key_lowercase}, {edit_values[key]} not in {details_address}"
        else:
            assert edit_values[key] == details[key.lower()], \
                f"Values {edit_values[key]} and {details[key.lower()]} don't match"
